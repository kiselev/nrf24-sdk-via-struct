/* NRF24L01+ SDK
 *
 * Minimalistic portable SDK for NRF24L01+ designed to be simple and as close to the datasheet, as possible.
 *
 * Author: Anton Kiselev(kisel)
 * 
 * requires following platform-dependent functions:
 * delay_us, nrf24_ce_set, nrf24_ce_clear, nrf24_spi_select, nrf24_spi_unselect, nrf24_spi_send
 * for details, see their declarations.
 *
 * */

#ifndef NRF24_DEFINED
#define NRF24_DEFINED

#include <stdint.h>

/* NRF24 structure is platform-specific */
struct NRF24;

/* sends raw SPI packet to nrf24. CSN is set during transmission */
void nrf24_spi_packet(struct NRF24 *nrf24, uint8_t *pTxData, uint8_t *pRxData, int sz);

/** send 1-byte command, returns status register */
uint8_t nrf24_command(struct NRF24 *nrf24, uint8_t cmd);

/** read 8bit register */
uint8_t nrf24_read_register_byte(struct NRF24 *nrf24, uint8_t reg);

/** write to 8bit register */
void nrf24_write_register_byte(struct NRF24 *nrf24, uint8_t reg, uint8_t val);

/** read register */
void nrf24_read_register(struct NRF24 *nrf24, uint8_t reg, uint8_t *val, uint8_t sz);

/** write register */
void nrf24_write_register(struct NRF24 *nrf24, uint8_t reg, uint8_t *val, uint8_t sz);

/** set/clear bit for 8bit nrf24 register */
void nrf24_register_setbit(struct NRF24 *nrf24, uint8_t reg, uint8_t bits_to_set, uint8_t bits_to_clear);

/** got to standby1 mode from power down or rx modes */
void nrf24_standby1(struct NRF24 *nrf24);

/* nrf24_transmit_cmd(W_TX_PAYLOAD) */
void nrf24_transmit(struct NRF24 *nrf24, uint8_t *data, int sz);

/* nrf24_transmit_cmd(W_TX_PAYLOAD_NOACK) */
void nrf24_transmit_noack(struct NRF24 *nrf24, uint8_t *data, int sz);

/** transmit data. standby1 mode. cmd is W_TX_PAYLOAD or W_TX_PAYLOAD_NOACK */
void nrf24_transmit_cmd(struct NRF24 *nrf24, uint8_t cmd, uint8_t *data, int sz);

/** wait for TX_EMPTY flag */
void nrf24_transmit_wait(struct NRF24 *nrf24, unsigned timeout_us);

/** go RX from standby1 mode */
void nrf24_rx_mode(struct NRF24 *nrf24);

/* returns the number of bytes received */
uint8_t nrf24_read_rx_payload(struct NRF24 *nrf24, uint8_t *data);

/* initalize GPIO to CE=0, CSN=1
required if there are multiple SPI devices on the same bus */
void nrf24_gpio_init(struct NRF24 *nrf24);

/* partial reset. restores original state of some registers.
mostly used as a reference for initial setup funcition
and for debugging purposes and testing invalid chips */
void nrf24_reset(struct NRF24 *nrf24);

//-------------
#define NRF24_SETTLING_US 130 /* settling delay for RX/TX */
#define NRF24_TX_PULSE_US 10 /* 10us CE HIGH pulse to start TX */

// --------------- Platform-dependent functions. to be defined by user ---------------------
/** CE = 1 */
void nrf24_ce_set(struct NRF24 *nrf24);
/** CE = 0 */
void nrf24_ce_clear(struct NRF24 *nrf24);
/** CSN = 0 */
void nrf24_spi_select(struct NRF24 *nrf24);
/** CSN = 1 */
void nrf24_spi_unselect(struct NRF24 *nrf24);
/* sends SPI data to nrf24. CSN unchanged */
void nrf24_spi_send(struct NRF24 *nrf24, uint8_t *pTxData, uint8_t *pRxData, int sz);


#endif //NRF24_DEFINED
