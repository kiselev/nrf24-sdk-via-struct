#include "nrf24.h"
#include "nrf24l01_defines.h"

#define NRF24_MAX_BUF 32 /* maximum register size is 32bytes */

// extracts RX_P_NO from STATUS register.
#define nrf24_rx_pipe_from_status(status) ((status >> 1) & 0x7)

void delay_us(unsigned);

/* sends raw SPI packet to nrf24. CSN is set during transmission */
void nrf24_spi_packet(struct NRF24 *nrf24, uint8_t *pTxData, uint8_t *pRxData, int sz)
{
  nrf24_spi_select(nrf24);
  nrf24_spi_send(nrf24, pTxData, pRxData, sz);
  nrf24_spi_unselect(nrf24);
}

/** send 1-byte command, returns status register */
uint8_t nrf24_command(struct NRF24 *nrf24, uint8_t cmd)
{
  uint8_t status;
  nrf24_spi_packet(nrf24, &cmd, &status, 1);
  return status;
}

/** read 8bit register */
uint8_t nrf24_read_register_byte(struct NRF24 *nrf24, uint8_t reg)
{
  uint8_t buf[2]={R_REGISTER|reg, NOP};
  nrf24_spi_packet(nrf24, buf, buf, 2);
  return buf[1];
}

/** write to 8bit register */
void nrf24_write_register_byte(struct NRF24 *nrf24, uint8_t reg, uint8_t val)
{
  uint8_t buf[2]={W_REGISTER|reg, val};
  nrf24_spi_packet(nrf24, buf, buf, 2);
}

/** read register */
void nrf24_read_register(struct NRF24 *nrf24, uint8_t reg, uint8_t *val, uint8_t sz)
{
  uint8_t buf[NRF24_MAX_BUF] = {R_REGISTER|reg};
  nrf24_spi_select(nrf24);
  nrf24_spi_send(nrf24, buf, buf, 1); /* send R_REGISTER|reg */
  nrf24_spi_send(nrf24, buf, val, sz); /* recv payload */
  nrf24_spi_unselect(nrf24);
}

/** write register */
void nrf24_write_register(struct NRF24 *nrf24, uint8_t reg, uint8_t *val, uint8_t sz)
{
  uint8_t buf[NRF24_MAX_BUF] = {W_REGISTER|reg};
  nrf24_spi_select(nrf24);
  nrf24_spi_send(nrf24, buf, buf, 1); /* send W_REGISTER|reg */
  nrf24_spi_send(nrf24, val, buf, sz); /* send value */
  nrf24_spi_unselect(nrf24);
}

/** set/clear bit for 8bit nrf24 register */
void nrf24_register_setbit(struct NRF24 *nrf24, uint8_t reg, uint8_t bits_to_set, uint8_t bits_to_clear)
{
  uint8_t val = nrf24_read_register_byte(nrf24, reg);
  val |= bits_to_set | bits_to_clear;
  val ^= bits_to_clear;
  nrf24_write_register_byte(nrf24, reg, val);
}

/** got to standby1 mode from power down or rx modes */
void nrf24_standby1(struct NRF24 *nrf24)
{
  nrf24_ce_clear(nrf24);
  nrf24_register_setbit(nrf24, CONFIG, 1<<PWR_UP, 0); /* cfg reg PWR_UP = 1 */
}

/* nrf24_transmit_cmd(W_TX_PAYLOAD) */
void nrf24_transmit(struct NRF24 *nrf24, uint8_t *data, int sz)
{
  nrf24_transmit_cmd(nrf24, W_TX_PAYLOAD, data, sz);
}

/* nrf24_transmit_cmd(W_TX_PAYLOAD_NOACK) */
void nrf24_transmit_noack(struct NRF24 *nrf24, uint8_t *data, int sz)
{
  nrf24_transmit_cmd(nrf24, W_TX_PAYLOAD_NOACK, data, sz);
}

/** transmit data. standby1 mode. cmd is W_TX_PAYLOAD or W_TX_PAYLOAD_NOACK */
void nrf24_transmit_cmd(struct NRF24 *nrf24, uint8_t cmd, uint8_t *data, int sz)
{
  uint8_t dummy[NRF24_MAX_BUF] = {0};

  nrf24_ce_clear(nrf24);
  /* PRIM_RX = 0 for TX mode */
  nrf24_register_setbit(nrf24, CONFIG, 1<<PWR_UP, 1<<PRIM_RX);
    
  /* set TX FIFO */
  nrf24_spi_select(nrf24);
  nrf24_spi_send(nrf24, &cmd, dummy, 1); /* send W_TX_PAYLOAD or W_TX_PAYLOAD_NOACK */
  nrf24_spi_send(nrf24, data, dummy, sz); /* send payload */
  nrf24_spi_unselect(nrf24);

  nrf24_ce_set(nrf24);
  delay_us(NRF24_TX_PULSE_US);
  nrf24_ce_clear(nrf24);
}

/** wait for TX_EMPTY flag */
void nrf24_transmit_wait(struct NRF24 *nrf24, unsigned timeout_us)
{
  do {
    uint8_t fifo_status = nrf24_read_register_byte(nrf24, FIFO_STATUS);
    if ( fifo_status & (1<<TX_EMPTY))
      return;
    delay_us(1);
  } while(timeout_us--);
}

/** go RX from standby1 mode */
void nrf24_rx_mode(struct NRF24 *nrf24)
{
  /* go RX Settling. for TX PRIM_RX=1 */
  nrf24_register_setbit(nrf24, CONFIG, 1<<PWR_UP | 1<<PRIM_RX, 0);
  nrf24_ce_set(nrf24);
}

/* returns the number of bytes received */
uint8_t nrf24_read_rx_payload(struct NRF24 *nrf24, uint8_t *data)
{
  uint8_t sz = 0;
  uint8_t cmd = R_RX_PAYLOAD;
  uint8_t status, pipe;
  nrf24_spi_select(nrf24);
  nrf24_spi_send(nrf24, &cmd, &status, 1);
  pipe = nrf24_rx_pipe_from_status(status); 
  if (pipe < 6) {
    /* pipe is valid index if data available on fifo or >=6 otherwise */
    sz = 32; /* read sz? */
    uint8_t buf[32]={0}; /* dummy buf, data will be ignored */
    nrf24_spi_send(nrf24, buf, data, sz);
  }
  nrf24_spi_unselect(nrf24);
  return sz;
}

/* initalize CE, CSN GPIO properly.
required if there are multiple SPI devices on the same bus */
void nrf24_gpio_init(struct NRF24 *nrf24)
{
  nrf24_spi_unselect(nrf24);
  nrf24_ce_clear(nrf24);
}

/* partial reset. restores original state of some registers.
mostly used as a reference for initial setup funcition
and for debugging purposes and testing invalid chips */
void nrf24_reset(struct NRF24 *nrf24)
{
  /* no constants here for flexibility. see datasheet for details */
  nrf24_command(nrf24, NOP); /* to ensure SPI works correctly */
  nrf24_command(nrf24, FLUSH_TX);
  nrf24_command(nrf24, FLUSH_RX);
  nrf24_write_register_byte(nrf24, CONFIG, 0x08);
  nrf24_write_register_byte(nrf24, STATUS, 0x0E);
  nrf24_write_register_byte(nrf24, DYNPD, 0);
  nrf24_write_register_byte(nrf24, FEATURE, 0);
  nrf24_write_register_byte(nrf24, EN_AA, 0x3f); /* 6 pipes mask */
  nrf24_write_register_byte(nrf24, EN_RXADDR, 0x03); /* ERX_P0|ERX_P1 */
  nrf24_write_register_byte(nrf24, SETUP_AW, 0x03); /* 5 bytes addr */
  nrf24_write_register_byte(nrf24, SETUP_RETR, 0x03); /* delay:250us 3 retransmits */
  nrf24_write_register_byte(nrf24, RF_CH, 0x02);
  nrf24_write_register_byte(nrf24, RF_SETUP, 0x0E); /* 2Mbps, 0dBm TX (max power) */
}

