#include "nrf24_stm32.h"

/** CE = 1 */
void nrf24_ce_set(struct NRF24 *nrf24)
{
  nrf24->gpio_port->ODR |= nrf24->ce;
}

/** CE = 0 */
void nrf24_ce_clear(struct NRF24 *nrf24)
{
  nrf24->gpio_port->ODR &= ~nrf24->ce;
}

/** CSN = 0 */
void nrf24_spi_select(struct NRF24 *nrf24)
{
    nrf24->gpio_port->ODR &= ~nrf24->csn;
}

/** CSN = 1 */
void nrf24_spi_unselect(struct NRF24 *nrf24)
{
  nrf24->gpio_port->ODR |= nrf24->csn;
}

/* sends SPI data to nrf24. CSN unchanged */
void nrf24_spi_send(struct NRF24 *nrf24, uint8_t *pTxData, uint8_t *pRxData, int sz)
{
  HAL_SPI_TransmitReceive(nrf24->hspi, pTxData, pRxData, sz, 0);
}

