/* NRF24L01+ SDK for STM32
 *
 * Minimalistic portable SDK for NRF24L01+ designed to be simple and as close to the datasheet, as possible.
 *
 * Author: Anton Kiselev(kisel)
 * 
 * requires 
 * - standart STM32 HAL library
 * - delay_us
 *
 * */

#include "platform.h"
#include "nrf24.h"

struct NRF24
{
  unsigned ce; /* CE gpio pin bitmask */
  unsigned csn; /* CSN gpio pin bitmask */
  SPI_HandleTypeDef *hspi; /* SPI port connected to NRF24 */
  GPIO_TypeDef *gpio_port; /* GPIO port for CE, CSN */
};

/* CE = 1 */
void nrf24_ce_set(struct NRF24 *nrf24);

/* CE = 0 */
void nrf24_ce_clear(struct NRF24 *nrf24);

/* CSN = 0 */
void nrf24_spi_select(struct NRF24 *nrf24);

/* CSN = 1 */
void nrf24_spi_unselect(struct NRF24 *nrf24);

/* sends SPI data to nrf24. CSN unchanged */
void nrf24_spi_send(struct NRF24 *nrf24, uint8_t *pTxData, uint8_t *pRxData, int sz);

